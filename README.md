# VoteMap Project

## Overview

VoteMap is a cutting-edge project aimed at visualizing United Nations voting data, offering insights into global voting patterns. It transforms complex voting information into accessible, interactive visualizations, facilitating a deeper understanding of international relations and decision-making.

## Features:

- **Data Extraction:** Automated scripts to harvest UN voting records.
- **Data Processing:** Advanced tools to refine and organize the extracted data.
- **Interactive Visualizations:** Engaging maps and charts highlighting voting trends.
- **User-Friendly Interface:** An intuitive web platform for easy data exploration.

## Export Metadata from UNDL

Under these links you find all the nessasary information how to use API

https://research.un.org/en/digitallibrary/export

https://ask.un.org/faq/330041

https://digitallibrary.un.org/help/hacking/record-api

UNDL uses MARC, Python library >> Pymarc
https://pymarc.readthedocs.io/en/latest/
https://www.loc.gov/marc/bibliographic/

Example:

https://digitallibrary.un.org/record/4030705

https://digitallibrary.un.org/record/4030705?of=xm&ot=992,952,993,996,967,245

XML-Data:
- **245	global_title**
  - a:	title
  - b:	type
  - c:	formal
- **952	meeting_record**
  - a:	id-meeting-record
- **967	vote_information**
  - a:	number
  - c:	short-country-name
  - d:	desicion
  - e:	full-country-name
- **992	action_date**
  - a:	date
- **993	related_documents**
  - a:	id-draft-resolution
- **996	vote_summary**
  - b:	yes
  - c:	no
  - d:	abstentions
  - e:	non-voting
  - f:	total-voting-membership