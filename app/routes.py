from flask import Blueprint, render_template, request, jsonify
from .models import db, Resolution, Vote, Country, ResolutionDocument

bp = Blueprint('main', __name__)

@bp.route('/')
def index():
    return render_template('index.html')

@bp.route('/resolutions')
def get_resolutions():
    resolutions = Resolution.query.all()
    return jsonify([res.to_dict() for res in resolutions])

@bp.route('/resolution/<int:id>')
def get_resolution(id):
    resolution = Resolution.query.get_or_404(id)
    return jsonify(resolution.to_dict())

@bp.route('/votes')
def get_votes():
    votes = Vote.query.all()
    return jsonify([vote.to_dict() for vote in votes])

@bp.route('/vote/<int:id>')
def get_vote(id):
    vote = Vote.query.get_or_404(id)
    return jsonify(vote.to_dict())

@bp.route('/countries')
def get_countries():
    countries = Country.query.all()
    return jsonify([country.to_dict() for country in countries])
