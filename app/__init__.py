from flask import Flask
from config import config_by_name
from app.models import db

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    
    db.init_app(app)
    
    with app.app_context():
        db.create_all()
    
    from app.routes import bp as main_bp
    app.register_blueprint(main_bp)
    
    return app
