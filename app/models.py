from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Resolution(db.Model):
    __tablename__ = 'resolutions'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    agenda_item = db.Column(db.String)
    resolution_number = db.Column(db.String)
    meeting_record = db.Column(db.String)
    draft_resolution = db.Column(db.String)
    committee_report = db.Column(db.String)
    vote_summary = db.Column(db.String)
    vote_date = db.Column(db.Date)
    notes = db.Column(db.Text)

    votes = db.relationship('Vote', backref='resolution', lazy=True)
    documents = db.relationship('ResolutionDocument', backref='resolution', lazy=True)

    def __repr__(self):
        return f'<Resolution {self.resolution_number} - {self.title}>'

    def to_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'agenda_item': self.agenda_item,
            'resolution_number': self.resolution_number,
            'meeting_record': self.meeting_record,
            'draft_resolution': self.draft_resolution,
            'committee_report': self.committee_report,
            'vote_summary': self.vote_summary,
            'vote_date': self.vote_date.isoformat() if self.vote_date else None,
            'notes': self.notes
        }

class Vote(db.Model):
    __tablename__ = 'votes'

    id = db.Column(db.Integer, primary_key=True)
    resolution_id = db.Column(db.Integer, db.ForeignKey('resolutions.id'), nullable=False)
    country = db.Column(db.String, nullable=False)
    vote = db.Column(db.String, nullable=False)  # Y, N, or A for Yes, No, or Abstention

    def __repr__(self):
        return f'<Vote {self.country} - {self.vote}>'

    def to_dict(self):
        return {
            'id': self.id,
            'resolution_id': self.resolution_id,
            'country': self.country,
            'vote': self.vote
        }

class Country(db.Model):
    __tablename__ = 'countries'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)

    def __repr__(self):
        return f'<Country {self.name}>'

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }

class ResolutionDocument(db.Model):
    __tablename__ = 'resolution_documents'

    id = db.Column(db.Integer, primary_key=True)
    resolution_id = db.Column(db.Integer, db.ForeignKey('resolutions.id'), nullable=False)
    document_type = db.Column(db.String, nullable=False)  # e.g., 'draft_resolution', 'committee_report'
    document_content = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return f'<ResolutionDocument {self.document_type} for Resolution {self.resolution_id}>'

    def to_dict(self):
        return {
            'id': self.id,
            'resolution_id': self.resolution_id,
            'document_type': self.document_type,
            'document_content': self.document_content
        }
