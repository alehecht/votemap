from app import create_app

# Auswahl der Konfigurationsumgebung: 'development', 'testing', 'production'
config_name = 'development'

# Erstelle die Flask-App mit der gewählten Konfiguration
app = create_app(config_name)

if __name__ == '__main__':
    app.run()
