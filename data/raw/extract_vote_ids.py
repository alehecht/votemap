import requests
from bs4 import BeautifulSoup
import csv
import re

# Base URL for pagination access
base_url = 'https://digitallibrary.un.org/search?ln=en&c=Voting+Data&rg=200&jrec={jrec}&cc=Voting+Data&fct__9=Vote'

# Empty list to collect IDs
vote_ids = []

# Set the starting point for pagination
jrec = 1
records_per_page = 200  # Entries per page

# Determine the total number of records dynamically
first_page_url = base_url.format(jrec=1)
response = requests.get(first_page_url)

if response.ok:
    soup = BeautifulSoup(response.content, 'html.parser')
    total_records_text = soup.find('div', {'class': 'hits'}).text
    total_records = int(re.search(r'of (\d+)', total_records_text).group(1))
else:
    raise Exception("Error fetching the total number of records.")

# Loop through all the pages
while jrec <= total_records:
    # Update the URL for the current page
    page_url = base_url.format(jrec=jrec)

    # Send the request and get the response
    response = requests.get(page_url)

    # Check the response
    if response.ok:
        # Parse the HTML content
        soup = BeautifulSoup(response.content, 'html.parser')
        
        # Find all links leading to voting details
        links = soup.find_all('a', href=True)
        
        # Extract IDs from the links
        for link in links:
            match = re.search(r'record/(\d+)', link['href'])
            if match:
                vote_ids.append(match.group(1))
    else:
        print(f"Error fetching page {jrec}")

    # Increment 'jrec' by 'records_per_page' for the next page
    jrec += records_per_page

# Write the IDs to a CSV file
with open('vote_ids.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['ID'])  # Header
    for vote_id in vote_ids:
        writer.writerow([vote_id])

print(f"IDs have been successfully extracted and saved to 'vote_ids.csv'.")
