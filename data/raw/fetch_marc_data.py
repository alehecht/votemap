import csv
import requests
import xml.etree.ElementTree as ET
import psycopg2

# Database connection parameters
db_params = {
    'dbname': 'your_database',
    'user': 'your_username',
    'password': 'your_password',
    'host': 'localhost'
}

# Function to extract data from XML
def extract_data_from_xml(xml_content, field_mapping):
    root = ET.fromstring(xml_content)
    extracted_data = {}
    for field in field_mapping:
        datafield = root.find(f".//datafield[@tag='{field}']")
        if datafield is not None:
            for subfield in field_mapping[field]:
                sf = datafield.find(f"subfield[@code='{subfield}']")
                if sf is not None:
                    extracted_data[field_mapping[field][subfield]] = sf.text
    return extracted_data

# Field mappings from XML to Database columns
field_mapping = {
    '245': {'a': 'title', 'b': 'type', 'c': 'formal'},
    '952': {'a': 'id-meeting-record'},
    '967': {'a': 'number', 'c': 'short-country-name', 'd': 'desicion', 'e': 'full-country-name'},
    '992': {'a': 'date'},
    '993': {'a': 'id-draft-resolution'},
    '996': {'b': 'yes', 'c': 'no', 'd': 'abstentions', 'e': 'non-voting', 'f': 'total-voting-membership'},
}

# Read vote IDs from CSV
vote_ids = []
with open('vote_ids.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        vote_ids.append(row['ID'])

# Connect to your PostgreSQL database
conn = psycopg2.connect(**db_params)
cur = conn.cursor()

# Loop through each vote ID and process XML
for vote_id in vote_ids:
    url = f'https://digitallibrary.un.org/record/{vote_id}?of=xm&ot=992,952,993,996,967,245'
    response = requests.get(url)
    if response.ok:
        data = extract_data_from_xml(response.content, field_mapping)
        
        # Construct the SQL query to insert data into the database
        # Assume your table is named 'votes' and columns match the field_mapping values
        columns = ', '.join(data.keys())
        values = ', '.join(['%s'] * len(data.values()))
        query = f"INSERT INTO votes ({columns}) VALUES ({values})"
        
        # Execute the query
        cur.execute(query, list(data.values()))
        
        # Commit the transaction
        conn.commit()

# Close the database connection
cur.close()
conn.close()
